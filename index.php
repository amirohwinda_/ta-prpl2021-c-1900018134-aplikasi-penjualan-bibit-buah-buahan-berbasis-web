<!DOCTYPE html>
<html>
<body>

<div class="content">
	<header>
		<h1 class="judul">Tugas PRPL</h1>
		<h3 class="deskripsi">Membuat Beranda Web</h3>
	</header>
 
	<div class="menu">
		<ul>
			<li><a href="index.php?page=home">HOME</a></li>
			<li><a href="index.php?page=dashboard">DASHBOARD</a></li>
			<li><a href="index.php?page=transaksi">TRANSAKSI</a></li>
		</ul>
	</div>
 
	<div class="badan">
 
 
	<?php 
	if(isset($_GET['page'])){
		$page = $_GET['page'];
 
		switch ($page) {
			case 'home':
				include "halaman/home.php";
				break;
			case 'dashboard':
				include "halaman/dashboard.php";
				break;
			case 'transaksi':
				include "halaman/transaksi.php";
				break;			
			default:
				echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
				break;
		}
	}else{
		include "halaman/home.php";
	}
 
	 ?>
 
	</div>
</div>
</body>
</html>